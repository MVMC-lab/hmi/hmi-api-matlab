from configparser import ConfigParser
import os

__all__ = ['ENV']

class Env(object):

    class _Gitlab(object):
        pid = str()
        pac = str()

    gitlab = _Gitlab()

    def __init__(self, file):
        super(Env, self).__init__()
        self.env_paser(file)
    
    def env_paser(self, file):
        cfg = ConfigParser()
        cfg.read(file)
        self.gitlab.pid = cfg['gitlab']['pid']
        self.gitlab.pac = cfg['gitlab']['pac']

ENV = Env('env/.env')

