import conf
import zipfile
import requests
import os
from env import ENV


VERSION = '0.4.2'
ZIP_FILE_NAME = f'hmi-matlab_api_v{VERSION}.zip'


def zip_lib_files():
    os.makedirs('dist', exist_ok=True)
    z = zipfile.ZipFile('dist/'+ZIP_FILE_NAME, mode='w')
    for name in os.listdir('./lib'):
        filename = f'./lib/{name}'
        z.write(filename, arcname=name)


def release():
    tag = VERSION
    pac = ENV.gitlab.pac
    pid = ENV.gitlab.pid

    # 創建 release 及 tag
    url = f'https://gitlab.com/api/v4/projects/{pid}/releases'
    r = requests.post(
        url=url,
        headers={
            'Private-Token': pac
        },
        params={
            'name': tag,
            'tag_name': tag,
            'ref': 'master',
            'description': '.'
        }
    )
    print('==== 創建tag結果 =================================================')
    print(r.json())

    # 上傳檔案
    filename = f'dist/{ZIP_FILE_NAME}'
    url = f'https://gitlab.com/api/v4/projects/{pid}/uploads'
    r = requests.post(
        url=url,
        headers={
            'Private-Token': pac,
        },
        files={
            'file': open(filename, 'rb')
        }
    )
    print('==== 上傳檔案結果 ================================================')
    print(r.json())

    name = r.json()['alt']
    url = r.json()['url']
    download_url = f'https://gitlab.com/MVMC-lab/hmi/hmi-api-matlab{url}'

    # 創建links
    url = f'https://gitlab.com/api/v4/projects/{pid}/releases/{tag}/assets/links'
    r = requests.post(
        url=url,
        headers={
            'Private-Token': pac
        },
        params={
            'name': name,
            'url': download_url
        }
    )
    print('==== 創建links結果 ===============================================')
    print(r.json())


def main():
    zip_lib_files()
    release()


if __name__ == '__main__':
    main()
