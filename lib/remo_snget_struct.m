function [data, err] = remo_snget_struct(port, data)

    s = readline(port);
    s = split(s,', ');
    if strcmp('~GS', s(1))
        writeline(port, '~ACK');
        pause(0.001);
        [data, err] = remo_get_struct(port);
    else
        error('sync failed');
        err = 6;
    end

end
