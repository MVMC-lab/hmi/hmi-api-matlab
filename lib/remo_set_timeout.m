function [err] = remo_set_timeout(port, timeout)
    set(port, 'Timeout', timeout);
    err = 0
end
