function [data, err] = remo_snget_array(port)

    s = readline(port);
    s = split(s,', ');
    if strcmp('~GA', s(1))
        writeline(port, '~ACK');
        pause(0.001);
        [data, err] = remo_get_array(port);
    else
        error('sync failed');
        err = 6;
    end

end
