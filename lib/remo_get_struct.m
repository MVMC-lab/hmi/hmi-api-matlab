function [data, fs, err] = remo_get_struct(port)

    err = 0;
    data = 0;

    % get 3bytes header 0xAC(172)
    for i = 1:3
        check = read(port, 1, 'uint8');

        if check ~= 172
            error('communication err (header), please check the process')
            err = 5;
            return
        end

    end

    % get packet length
    h = read(port, 1, 'uint8');
    l = read(port, 1, 'uint8');
    packet_length = (h * 256) + l;

    raw = read(port, packet_length, 'uint8');
    chksum = read(port, 1, 'uint8');

    % check chksum is matched
    if chksum ~= rem(sum(raw), 256)
        error('communication err (chksum), please check the process')
        err = 5;
        return
    end

    if packet_length <= 6
        error('communication err (packet length), please check the process')
        err = 5;
        return
    end

    % packet type
    % PAC_TYPE_ST is 3
    if raw(1) ~= 3
        error('communication err (packet type), please check the process')
        err = 5;
        return
    end

    fs_length = raw(2);
    fs = char(raw(3:2 + fs_length)');
    [Format, TypeNum, StructSize, fs_err] = decodeFormatStr(fs);

    if fs_err ~= 0
        error('communication err (format string), please check the process')
        err = 5;
        return
    end

    data_size = raw(3 + fs_length) * 256 + raw(4 + fs_length);
    bytedata = uint8(raw(5 + fs_length:packet_length));

    % trans binary data to struct
    i = 1;

    for n = 1:data_size / StructSize

        for j = 1:TypeNum
            l = i + Format(j).TypeSize * Format(j).Num - 1;
            arraydata = typecast(uint8(bytedata(i:l)), Format(j).Type);
            i = l + 1;
            celldata(j, n) = {arraydata};
        end

    end

    data = cell2struct(celldata, {Format.Name}, 1);

    return
end
