function [err] = remo_snput_matrix(port, data)

    s = readline(port);
    s = split(s,', ');
    if strcmp('~PM', s(1))
        writeline(port, '~ACK');
        pause(0.001);
        err = remo_put_matrix(port, data);
    else
        error('sync failed');
        err = 6;
    end

end
