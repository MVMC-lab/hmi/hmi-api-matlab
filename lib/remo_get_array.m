function [data, err] = remo_get_array(port)

    err = 0;
    data = 0;

    % get 3bytes header 0xAC(172)
    for i = 1:3
        check = read(port, 1, 'uint8');

		if check ~= 172
			error('communication err (header), please check the process')
            err = 5;
            return
        end

    end

    % get packet length
    h = read(port, 1, 'uint8');
    l = read(port, 1, 'uint8');
    packet_length = (h * 256) + l;

    raw = read(port, packet_length, 'uint8');
    chksum = read(port, 1, 'uint8');

    % check chksum is matched
    if chksum ~= rem(sum(raw), 256)
        error('communication err (chksum), please check the process')
        err = 5;
        return
    end

    if packet_length <= 5
        error('communication err (packet length), please check the process')
        err = 5;
        return
    end

    % packet type
    % PAC_TYPE_AR is 1
    if raw(1) ~= 1
        error('communication err (packet type), please check the process')
        err = 5;
        return
    end

    % data type
    [data_type, type_size] = get_type(raw(2));

    data_num = raw(3);
    data_size = raw(4) * 256 + raw(5);

    if data_size ~= data_num * type_size
        error('communication err (data size), please check the process')
        err = 5;
        return
    end

    bytedata = uint8(raw(6:packet_length));

    % trans binary data to assigned data_type
    data = typecast(uint8(zeros(1, data_size)), data_type);

    for i = 1:data_num
        s = (i - 1) * type_size + 1;
        e = s + type_size - 1;
        data(i) = typecast(bytedata(s:e), data_type);
    end

    return
end

function [data_type, type_size] = get_type(num)

    switch num
        case 0
            % int8
            data_type = 'int8';
            type_size = 1;
        case 1
            % int16
            data_type = 'int16';
            type_size = 2;
        case 2
            % int32
            data_type = 'int32';
            type_size = 4;
        case 3
            % int64
            data_type = 'int64';
            type_size = 8;
        case 4
            % uint8
            data_type = 'uint8';
            type_size = 1;
        case 5
            % uint16
            data_type = 'uint16';
            type_size = 2;
        case 6
            % uint32
            data_type = 'uint32';
            type_size = 4;
        case 7
            % uint64
            data_type = 'uint64';
            type_size = 8;
        case 8
            % float32
            data_type = 'single';
            type_size = 4;
        case 9
            % float64
            data_type = 'double';
            type_size = 8;
        otherwise
            error('error data type');
            return
    end
end
