function [err] = remo_snput_struct(port, data)

    s = readline(port);
    s = split(s,', ');
    if strcmp('~PS', s(1))
        writeline(port, '~ACK');
        pause(0.001);
        err = remo_put_struct(port, data);
    else
        error('sync failed');
        err = 6;
    end

end
