function [err] = remo_put_struct(port, data)

    err = 0;
    CheckSum = 0;

	[fs, data_size] = getFormatOfStruct(data)
    [Format, TypeNum, StructSize, err] = decodeFormatStr(fs);
    TypeNum = length(Format)
	fs_size = length(fs)

    % put 3bytes header 0xAC(172)
    write(port, 172, 'uint8');
    write(port, 172, 'uint8');
    write(port, 172, 'uint8');

    % put packet size
    packet_size = 4 + fs_size + data_size;
    h = floor(packet_size / 256);
    l = rem(packet_size, 256);
    write(port, h, 'uint8');
    write(port, l, 'uint8');

    % put packet type
    % PAC_TYPE_ST is 3
    write(port, 3, 'uint8');

    write(port, fs_size, 'uint8');
    write(port, fs, 'uint8');

    h = floor(data_size / 256);
    l = rem(data_size, 256);
    write(port, h, 'uint8');
	write(port, l, 'uint8');
				
    CheckSum = 3 + fs_size + sum(fs) + h + l;

    % put data
    names = fieldnames(data);

    for i = 1:TypeNum
        putdata = cast(getfield(data, names{i}), Format(i).Type);
        Cdata = typecast(putdata, 'uint8');
        write(port, Cdata, 'uint8');
        CheckSum = CheckSum + sum(Cdata);
    end

    % put CheckSum
    CheckSum = rem(CheckSum, 256);
    write(port, CheckSum, 'uint8');

end
