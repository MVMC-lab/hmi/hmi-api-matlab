function [data, err] = remo_snget_matrix(port)

    s = readline(port);
    s = split(s,', ');
    if strcmp('~GM', s(1))
        writeline(port, '~ACK');
        pause(0.001);
        [data, err] = remo_get_matrix(port);
    else
        error('sync failed');
        err = 6;
    end

end
