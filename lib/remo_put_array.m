function [err] = remo_put_array(port, data)

    err = 0;
    typeStr = class(data);

    if (strcmp(typeStr, 'int8'))
        data_type = 0;
        typeSize = 1;
        bytedata = typecast(int8(data), 'uint8');
    elseif (strcmp(typeStr, 'int16'))
        data_type = 1;
        typeSize = 2;
        bytedata = typecast(int16(data), 'uint8');
    elseif (strcmp(typeStr, 'int32'))
        data_type = 2;
        typeSize = 4;
        bytedata = typecast(int32(data), 'uint8');
    elseif (strcmp(typeStr, 'int64'))
        data_type = 3;
        typeSize = 8;
        bytedata = typecast(int64(data), 'uint8');
    elseif (strcmp(typeStr, 'uint8'))
        data_type = 4;
        typeSize = 1;
        bytedata = typecast(uint8(data), 'uint8');
    elseif (strcmp(typeStr, 'uint16'))
        data_type = 5;
        typeSize = 2;
        bytedata = typecast(uint16(data), 'uint8');
    elseif (strcmp(typeStr, 'uint32'))
        data_type = 6;
        typeSize = 4;
        bytedata = typecast(uint32(data), 'uint8');
    elseif (strcmp(typeStr, 'uint64'))
        data_type = 7;
        typeSize = 8;
        bytedata = typecast(uint64(data), 'uint8');
    elseif (strcmp(typeStr, 'single'))
        data_type = 8;
        typeSize = 4;
        bytedata = typecast(single(data), 'uint8');
    elseif (strcmp(typeStr, 'double'))
        data_type = 9;
        typeSize = 8;
        bytedata = typecast(double(data), 'uint8');
    else
        error('unsupported data type!');
        err = 1;
        return;
    end

    data_num = length(data);
    data_size = length(bytedata);

    if data_size > 65535
        error('data size exceeds 65535');
        err = 2;
        return;
    elseif data_num > 255
        error('data num exceeds 255');
        err = 3;
        return;
    end

    % put 3bytes header 0xAC(172)
    write(port, 172, 'uint8');
    write(port, 172, 'uint8');
	write(port, 172, 'uint8');
								
	% put packet size
	packet_size = 5 + data_size;
    h = floor(packet_size / 256);
	l = rem(packet_size, 256);
    write(port, h, 'uint8');
	write(port, l, 'uint8');

    % put packet type
    % PAC_TYPE_AR is 1
    write(port, 1, 'uint8');

    write(port, data_type, 'uint8');
				write(port, data_num, 'uint8');
				
    h = floor(data_size / 256);
    l = rem(data_size, 256);
    write(port, h, 'uint8');
	write(port, l, 'uint8');
	write(port, bytedata, 'uint8');
				
    chksum = 1 + data_type + data_num + h + l + sum(bytedata);
    chksum = rem(chksum, 256);
    write(port, chksum, 'uint8');

end
